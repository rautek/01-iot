# IoT Course Material #
This is a collection of usefull links, code snippets, and command line instructions that are used during this course. 
We will use it to get the Adafruit Huzzah ESP32, with Mongoose OS Eclipse IoT running.

Here is the slide deck of the IoT part of the course:
https://www.dropbox.com/s/ejg6bu76ky83jyw/mongoose-os-v01.pptx?dl=0

There is another [page for the related course on Conversational Interfaces and Chatbots](https://github.com/yipcma/bot-wep2018).

# MCU Soft and Hardware #

## Installing Mongoose OS ##
* [Download](https://mongoose-os.com/software.html)
* Play with the commandline
```
mos --help
```
* Play with the GUI
```
mos --ui
```

## Feather Huzzah ESP32 ##
* [Documentation](https://learn.adafruit.com/adafruit-huzzah32-esp32-feather/pinouts?view=all)

# MQTT #

## Eclipse IoT ##
* [Link](https://iot.eclipse.org/)
* MQTT-Server: iot.eclipse.org:1883

## MQTTbox ##
* [Installer](http://workswithweb.com/html/mqttbox/installing_apps.html)

# Code Snippets

* Subscribe and parse JSON 

```
let topicSub = "/devices/" + Cfg.get("device.id") + "/test";
MQTT.sub(topicSub, function(conn, top, msg) {  
	print("got message:", msg.slice(0, 100));  
	let obj = JSON.parse(msg);  
	if (obj) {    
		GPIO.write(led, obj.on);    
		print(obj.on ? "on" : "off");  
	} }, null);
```	


# Advanced Topics #
The advanced section goes beyond the topics of the course, but can be very helpful for projects during the hackathon.

## Google Cloud SDK and IoT Core ##
* [Google Cloud SDK Installer](https://cloud.google.com/sdk/downloads)
* [The steps for mongoose and google iot core integration in general](https://mongoose-os.com/docs/cloud_integrations/gcp.html)

### Steps Specifically for this Course ###
* Program the ESP32 with the [IoT Course Firmware](https://www.dropbox.com/s/x97modyuumca3mc/iot-course-01.zip?dl=0) 

* Choose values for MY_TOPIC and MY_REGISTRY
* Authenticate with Google Cloud:
```
gcloud auth login
```
* Set gcloud project:
```
gcloud config set project iot-course-01
```
* Create PubSub topic:
```
gcloud beta pubsub topics create MY_TOPIC
```
* Create PubSub subscription:
```
gcloud beta pubsub subscriptions create --topic MY_TOPIC MY_TOPIC-subscription
```
* Create device registry:
```
gcloud beta iot registries create MY_REGISTRY --region europe-west1 --event-pubsub-topic=MY_TOPIC
```
* Setup WiFi:
```
mos wifi WIFI_NETWORK_NAME WIFI_PASSWORD
```
* Register device on Google IoT Core.
```
mos gcp-iot-setup --gcp-project iot-course-01 --gcp-region europe-west1 --gcp-registry MY_REGISTRY
```

